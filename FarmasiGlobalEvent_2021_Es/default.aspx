<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="LandingPage_Default" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Farmasi Global Event 2021</title>
    <link rel="icon" href="img/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/globalconvention2021es/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/globalconvention2021es/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/globalconvention2021es/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/globalconvention2021es/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/globalconvention2021es/css/theme-cedar.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/globalconvention2021es/css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">
</head>
<body data-smooth-scroll-offset="77">
    <div class="nav-container">
        <div class="via-1600197052799" via="via-1600197052799" vio="Farmasi">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-9 col-md-10 text-right">
                            <a href="#hero" class="hamburger-toggle" data-toggle-class="#menu2;hidden-xs hidden-sm">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <nav id="menu2" class="bar bar-2 hidden-xs bg--dark bar--absolute">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 order-lg-1">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <a href="https://www.farmasius.com/" title="">
                                            <img src="img/farmasi-rednwhite.png" alt="" class="img-fluid">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <div class="main-container">
        <a id="hero" class="in-page-link" data-scroll-id="hero"></a>
        <section class="imageblock switchable height-100 d-block img-fluid">
            <img alt="image" src="/globalconvention2021es/img/Section_01_es.png" class="img-fluid" id="topIMG" usemap="#vfxlipstickset">
        </section>
        <a id="video" class="in-page-link" data-scroll-id="video"></a>
        <a id="shop" class="in-page-link" data-scroll-id="shop"></a>
        <a id="video" class="in-page-link" data-scroll-id="video"></a>
        <!--
        <section class="imagebg videobg text-center hidden-sm hidden-xs height-140">
            <video autoplay="" loop="" muted="">
                <source src="video/vfxlipstick.mp4" type="video/webm">
                <source src="video/vfxlipstick.mp4" type="video/mp4">
            </video>
            <div class="background-image-holder"> <img alt="image" src="img/backy.jpg"> </div>
        </section>

        <section class="text-center imagebg hidden-md hidden-lg" data-overlay="4">
            <div class="background-image-holder"> <img alt="background" src="img/backy2.png"> </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-10">
                        <div class="video-cover border--round">
                            <div class="background-image-holder"> <img alt="image" src="img/1303783_RETRO_ROSE.jpg"> </div>
                            <div class="video-play-icon"></div> <iframe data-src="https://www.youtube.com/embed/zoIyFUE2MEo" allowfullscreen="allowfullscreen"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->

        <section class="imageblock switchable height-100 d-block img-fluid">
            <img alt="image" src="/globalconvention2021es/img/Section_02_es.png" class="img-fluid">
        </section>
        <section class="imageblock switchable height-100 d-block">
            <img alt="image" src="/globalconvention2021es/img/Section_03_es.png" class="img-fluid" >
        </section>
        <section class="imageblock switchable height-100 d-block">
            <img alt="image" src="/globalconvention2021es/img/Section_04_es.png" class="img-fluid" >
        </section>
        <a id="about" class="in-page-link" data-scroll-id="about"></a>
      <footer class="footer-7 text-center-xs bg--dark">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <span class="type--fine-print">&copy; <span class="update-year">2020</span> Farmasi</span>
                    </div>
                    <div class="col-sm-6 text-right text-center-xs">
                        <ul class="social-list list-inline">
                            <li>
                                <a href="https://www.youtube.com/channel/UCa0BWzAqhRML6yG5CRRBhNQ?view_as=subscriber"
                                   target="_self"><i class="socicon icon socicon-youtube icon--xs"></i></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/Farmasi-USA-2234387646887335/?modal=admin_todo_tour"
                                   target="_self"><i class="socicon socicon-facebook icon icon--xs"></i></a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/farmasiusa/" target="_self">
                                    <i class="socicon socicon-instagram icon icon--xs"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <script src="/globalconvention2021es/js/jquery-3.1.1.min.js"></script>
    <script src="/globalconvention2021es/js/flickity.min.js"></script>
    <script src="/globalconvention2021es/js/parallax.js"></script>
    <script src="/globalconvention2021es/js/smooth-scroll.min.js"></script>
    <script src="/globalconvention2021es/js/jquery.rwdImageMaps.min.js"></script>
    <script src="/globalconvention2021es/js/scripts.js"></script>
    <script>
        $(document).ready(function (e) {
            $('img[usemap]').rwdImageMaps();
        });
    </script>
</body>
